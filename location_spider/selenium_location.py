import pandas as pd
import os
import time
from selenium import webdriver
from sys import argv

def setup():
    executable_path = r'/home/yajie/opt/chromedriver'
    chrome_options = webdriver.ChromeOptions()
    """后台运行Chromedriver"""
    chrome_options.add_argument('--headless')
    chrome_options.add_argument('--no-sandbox')
    chrome_options.add_argument('--start-maximized')
    browser = webdriver.Chrome(executable_path=executable_path, chrome_options=chrome_options)
    """全屏显示"""
    browser.maximize_window()
    # time.sleep(5)
    browser.get('http://api.map.baidu.com/lbsapi/getpoint/index.html')
    return browser

def searchOnline(browser, target):

    # 定位搜索框、清空内容、输入内容
    browser.switch_to.window(browser.window_handles[-1])
    search_key = browser.find_element_by_xpath('//*[@id="localvalue"]')
    search_key.clear()
    search_key.send_keys(target)

    # 搜索、点击任意位置获取坐标
    browser.find_element_by_xpath('//*[@id="localsearch"]').click()
    time.sleep(3)
    browser.find_element_by_xpath('//*[@id="no_0"]/p').click()


    # 获取经纬度坐标
    browser.switch_to.window(browser.window_handles[-1])
    location = browser.find_element_by_xpath('//*[@id="pointInput"]').get_attribute('data-clipboard-text')
    content = browser.find_element_by_xpath('//*[@id="no_0"]/a').text

    location = location.split(',')
    # browser.quit()
    print(target)
    print([float(location[0]), float(location[1])].__str__() + content)
    # print(content)

    return [float(location[0]), float(location[1])]

def findLatLong(browser, target_list):

    latlonglist = []
    for item in target_list:

        try:
            latlonglist.append(searchOnline(browser, item))
        except:
            print(item)
            latlonglist.append('')
    return latlonglist


def main():

    start_time = time.time()

    source_path = os.path.dirname(os.path.abspath(__file__))+'/Source'
    target_path = os.path.dirname(os.path.abspath(__file__))+'/Target'

    browser = setup()
    
    filename = argv[1]

    data_list = os.listdir(source_path)
    #for data in data_list:
    try:
        source_data = pd.read_csv(source_path + '/' + filename, encoding="GBK")
    except:
        source_data = pd.read_csv(source_path + '/' + filename, encoding="UTF-8")
    target_list = list(source_data.Label)
    latlong_list = findLatLong(browser, target_list)
    source_data.insert(source_data.columns.values.tolist().index('Label')+1, 'latlong', latlong_list)
    source_data.to_csv(target_path + '/' + filename, index=False, encoding='GBK')

    browser.quit()
    end_time = time.time()  # 结束时间
    cost_time = end_time - start_time  # 总运行时间，并按时分秒完成输出
    print('本次运行耗时：{0}时{1}分{2}秒 '.format(int(cost_time / 3600), int((cost_time / 60) % 60), int(cost_time % 60)))


if __name__ == '__main__':

    main()
